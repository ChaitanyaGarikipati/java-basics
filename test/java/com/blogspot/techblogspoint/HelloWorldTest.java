package com.blogspot.techblogspoint;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Test.None;

public class HelloWorldTest {
	HelloWorld hw = new HelloWorld();

	@Test(timeout = 100)
	public void test() {
		assertEquals("Public string can be accessed from anywhere.", hw.publicString);
	}
	
	@Test(expected = Exception.class)
	public void throwExceptionTest() throws Exception {
		throw new Exception("Throwed exception");
	}
	
	@Test(expected = None.class)
	public void accessPrivateVarsTest() {
		hw.accessPrivateVars();
	}

}
