package com.blogspot.techblogspoint;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ HelloWorldTest.class, JunitAnnotation.class, JunitParamTest.class })
public class JunitTestSuite {

}
