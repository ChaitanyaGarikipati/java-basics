package com.blogspot.techblogspoint.strings_and_arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StringPlayground {
	static void playWithString() {
		String s1 = "Welcome";
		String s2 = new String("Welcome");
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s2);
		
		System.out.println("============");
		
		String s3 = "Welcome";
		System.out.println(s1.equals(s3));
		System.out.println(s1 == s3);
		
		System.out.println("============");
		
		String s4 = s2;
		System.out.println(s2 == s4);
		s4 += " a small change";
		System.out.println(s2 == s4);
	}
	
	
	static void playWithSB() {

	}
	
	public static void main(String[] args) {
//		List<Integer> a = new ArrayList<Integer>();
//		List<Integer> b = a;
//		a.add(1234);
//		System.out.println(a==b);
//		
//		b.add(2);
//		System.out.println(a==b);
		
//		playWithString();
		
		
		int[] integerArray = {1, 2, 3};
		
		String[] stringArray = new String[] {"hi"};
		
		System.out.println(new int[] {1, 2, 3});
		
		
		String[][] stringMulDArr = new String[][] {
			new String[] {"hi", "english"},
			new String[] {"holla", "french"}
		};
		
		/**
		 * [
		 * 	["hi", "english"],
		 * 	["holla", "french"],
		 * ]
		 */
	}
}
