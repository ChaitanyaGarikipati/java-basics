package com.blogspot.techblogspoint;

import java.util.Scanner;

public class HelloWorld {
	public String publicString = "Public string can be accessed from anywhere.";
	private String privateString = "Public string can be accessed only inside current class.";
	protected String protectedString = "Protected string cannot be accessed outside package.";
	String defaultString = "Default String can be access inside current class and within package.";
	private char[] password;

	void accessPrivateVars() {
		this.privateString = "";
		this.defaultString = "";
	}


	public static void main(String[] args) {
		// java -jar HelloWorld.class --type=calendar --year=2022
		System.out.println("Hello World !!");
	}
}


class IO {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String ip = sc.next();
		System.out.println("Typed: "+ ip);
		sc.close();
	}
}

class RecursionExample {
	
	static void printTimes(int no) {
		if (no > 0) {
			System.out.println("Printing for "+ no + " times.");
			printTimes(--no);
		}
	}
	
	public static void main(String[] args) {
		printTimes(4);
	}
}