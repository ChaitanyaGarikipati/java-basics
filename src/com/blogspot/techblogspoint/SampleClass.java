package com.blogspot.techblogspoint;

public class SampleClass {
	private Object instance;
	
	public static void main(String[] args) {
		SampleClass s1 = new SampleClass();
		SampleClass s2 = new SampleClass();
		s1.instance = s2;
		s2.instance = s1;
	}
}