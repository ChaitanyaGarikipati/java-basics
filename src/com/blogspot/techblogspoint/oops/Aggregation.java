package com.blogspot.techblogspoint.oops;

public class Aggregation {
    public static void main(String args[]) {

        //Create Person object independently
        Person p = new Person();

        //Create the Organisation independently
        Organisation o = new Organisation();
        o.name = "XYZ Corporation";

        /*
          At this point both person and organisation 
          exist without any association  
        */
        p.worksFor = o;

    }
}

class Person {
    Organisation worksFor;
}

class Organisation {
    String name;
}

