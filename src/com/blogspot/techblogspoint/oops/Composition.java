package com.blogspot.techblogspoint.oops;

public class Composition {
	public static void main() {
        //Create Car object independently
        Car car = new Car();

        //Cannot create Wheel instance independently
        //need a reference of a Car for the same.
        Car.Wheel wheel = car.new Wheel();
    }
}

class Car {
    class Wheel {
        Car associatedWith;
    }
}
