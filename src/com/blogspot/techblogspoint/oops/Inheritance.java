package com.blogspot.techblogspoint.oops;

public class Inheritance {
	public static void main(String[] args) {
		Dog gs = new GermanShepard();
		System.out.println(gs);
		gs.move();
		
		
		Tiger tiger = new BengalTiger();
		System.out.println(tiger);
		tiger.move();
		tiger.makeSound(true);
	}
}

// polymorphism
// inheritance
// Abstraction
// encapsulation


interface Animal {
	void makeSound();
	void move();
	default void dance() {
		System.out.println("Animal Dancing !!");
	}
}

abstract class Dog implements Animal {
	private String breed;
	private String color;
	
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	@Override
	public void makeSound() {
		System.out.println("Bark!");
	}
	
	@Override
	public void move() {
		System.out.println("Dog Walks with 4 legs");
	}
	
	
	@Override
	public String toString() {
		return "Dog [breed=" + breed + ", color=" + color + "]";
	}
}

class GermanShepard extends Dog {
	public GermanShepard() {
		super();
		this.setBreed("German Shepard");
		this.setColor("Mixed brown and black");
	}
}

class Country {
	private double latitude;
	private double longitude;
	private double area;
	
	public Country(double latitude, double longitude, double area) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.area = area;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	@Override
	public String toString() {
		return "Country [latitude=" + latitude + ", longitude=" + longitude + ", area=" + area + "]";
	}
}

class India extends Country {
	public India() {
		super(123.4, 213.4, 563D);
	}
}

abstract class Tiger implements Animal {
	private Country nativeCountry; // association.
	
	public Country getNativeCountry() {
		return nativeCountry;
	}

	public void setNativeCountry(Country nativeCountry) {
		this.nativeCountry = nativeCountry;
	}

	@Override
	public void makeSound() {
		System.out.println("Roar");
	}
	
	@Override
	public void move() {
		System.out.println("Tiger Walks with 4 legs");
	}
	
	public void makeSound(boolean claimArea) {
		System.out.println("Roaring to claim area.");
	}

	@Override
	public String toString() {
		return "Tiger [nativeCountry=" + nativeCountry + "]";
	}
}


class BengalTiger extends Tiger {
	public BengalTiger() {
		super();
		this.setNativeCountry(new India());
	}
}