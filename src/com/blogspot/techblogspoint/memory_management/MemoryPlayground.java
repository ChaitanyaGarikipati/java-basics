package com.blogspot.techblogspoint.memory_management;

import java.util.ArrayList;
import java.util.List;

import com.blogspot.techblogspoint.HelloWorldTest;


public class MemoryPlayground {

	static void crash() {
		List<Object> objs = new ArrayList<>();
		while(true) {
			objs.add(new Object());
			System.out.println("Added new object to heap. New size is "+ objs.size());
		}
	}
	
	static void works() {
		int count = 0;
		while(true) {
			new Object();
			System.out.println("Added new object to heap. New size is "+ ++count);
		}
	}
	
	public static void main(String[] args) {
		HelloWorldTest hw = new HelloWorldTest();
//		hw.
//		hw.publicString = "";
//		crash();
//		works();
	}
}

