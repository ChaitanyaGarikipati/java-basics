package com.blogspot.techblogspoint.singleton;

import java.io.Serializable;

public class BillPughSingleton2 implements Serializable {
    private static final long serialVersionUID = 1L;

    private BillPughSingleton2() {}

    private static class SingletonHelper {
        private static final BillPughSingleton2 INSTANCE = new BillPughSingleton2();
    }

    public static BillPughSingleton2 getInstance() {
        return SingletonHelper.INSTANCE;
    }

    protected Object readResolve() {
        return SingletonHelper.INSTANCE;
    }
}