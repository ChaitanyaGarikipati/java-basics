package com.blogspot.techblogspoint.design_patterns.factory;

public class EmailService implements NotificationService {
	private String fromAdd;
	
	public EmailService(String fromAdd) {
		this.fromAdd = fromAdd;
	}

	@Override
	public void notifyUser() {
		System.out.println("Sending an Email notification with from address:" + fromAdd);
	}

}
