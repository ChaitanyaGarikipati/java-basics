package com.blogspot.techblogspoint.design_patterns.factory;

public class PushService implements NotificationService {

	@Override
	public void notifyUser() {
		System.out.println("Sending an Push notification");
	}

}
