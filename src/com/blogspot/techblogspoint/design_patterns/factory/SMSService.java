package com.blogspot.techblogspoint.design_patterns.factory;

public class SMSService implements NotificationService {
	private String refereceId;
	
	public SMSService(String referenceId) {
		this.refereceId = referenceId;
	}
	

	@Override
	public void notifyUser() {
		System.out.println("Sending an SMS notification");
	}

}
