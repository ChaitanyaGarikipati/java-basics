package com.blogspot.techblogspoint.design_patterns.factory;

public class NotificationServiceTest {
	public static void main(String[] args) {
		NotificationServiceFactory notificationServiceFactory = new NotificationServiceFactory();
		NotificationService notificationService = notificationServiceFactory.getNotificationService("SMS");
		notificationService.notifyUser();
		
		notificationService = notificationServiceFactory.getNotificationService("EMAIL");
		notificationService.notifyUser();
		
		
//		SMSService smsService = new SMSService();
//		smsService.notifyUser();
//		EmailService emailService = new EmailService("erlgk");
//		emailService.notifyUser();
	}
}
