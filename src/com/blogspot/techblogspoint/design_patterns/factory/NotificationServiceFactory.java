package com.blogspot.techblogspoint.design_patterns.factory;

public class NotificationServiceFactory {
	public NotificationService getNotificationService(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("SMS".equals(channel)) {
			return new SMSService("techblogspoint");
		} else if ("EMAIL".equals(channel)) {
			return new EmailService("admin@techblogspoint.com");
		} else if ("PUSH".equals(channel)) {
			return new PushService();
		}
		return null;
	}
}
