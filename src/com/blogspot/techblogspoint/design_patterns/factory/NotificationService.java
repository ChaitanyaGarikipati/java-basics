package com.blogspot.techblogspoint.design_patterns.factory;

public interface NotificationService {
	void notifyUser();
}
