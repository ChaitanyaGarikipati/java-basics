package com.blogspot.techblogspoint.design_patterns.dao;

import java.util.List;

public interface DAO<T> {
	T get(int id);

	List<T> getAll();

	T save(T t);

	T update(T t);

	boolean delete(T t);
}
