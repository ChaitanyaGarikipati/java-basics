package com.blogspot.techblogspoint.design_patterns.dao;

import java.util.ArrayList;
import java.util.List;

public class UserDAO implements DAO<User> {
	private List<User> usersDatasource = new ArrayList<>();

	public UserDAO() {
		usersDatasource.add(new User(1, "John", "john@domain.com"));
		usersDatasource.add(new User(2, "Susan", "susan@domain.com"));
	}

	@Override
	public User get(int id) {
		return usersDatasource.get(id); // select * from user where user_id=${id}, user.get(${id})
	}

	@Override
	public List<User> getAll() {
		return usersDatasource;
	}

	@Override
	public User save(User t) {
		usersDatasource.add(t);
		return t;
	}

	@Override
	public User update(User t) {
		usersDatasource.set(t.getId(), t);
		return t;
	}

	@Override
	public boolean delete(User t) {
		usersDatasource.remove(t);
		return true;
	}

}
