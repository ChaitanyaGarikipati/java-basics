package com.blogspot.techblogspoint.design_patterns.dao;

public class DAOTest {
	private static DAO<User> userDao;

	public static void main(String[] args) {
		userDao = new UserDAO();
		User user0 = userDao.get(0);
		User user1 = userDao.save(new User(3, "Julie", "julie@domain.com"));
		userDao.getAll().forEach(user -> System.out.println(user.getName()));
		userDao.delete(user1);
	}
}
