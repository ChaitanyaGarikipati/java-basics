package com.blogspot.techblogspoint.design_patterns;

import java.util.List;

public class ObjectCoupling {
	public static void main(String[] args) {
		Tortoise tortoise = new Tortoise();
		TightlyCoupledExmaple t = new TightlyCoupledExmaple(tortoise);
		t.move();
		
		LooslyCoupledExample l = new LooslyCoupledExample();
		for (Animal animal : List.of(new Dog(), new Parrot())) {
			l.setAnimal(animal);
			l.move();
		}
	}
}

class TightlyCoupledExmaple {
	private Tortoise tortoise;

	public TightlyCoupledExmaple(Tortoise tortoise) {
		this.tortoise = tortoise;
	}

	public void move() {
		tortoise.walkAway();
	}
}

class LooslyCoupledExample {
	private Animal animal;

	public void move() {
		animal.moveAway();
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}
}

class Tortoise {
	public void walkAway() {
		System.out.println("Tortoise walking away ....");
	}
}

interface Animal {
	void moveAway();
}

class Dog implements Animal {

	@Override
	public void moveAway() {
		System.out.println("Dog running away");
	}

}

class Parrot implements Animal {

	@Override
	public void moveAway() {
		System.out.println("Parrot flying away");
	}

}