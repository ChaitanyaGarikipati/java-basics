package com.blogspot.techblogspoint;

public final class ImmutableClass {
	private final int rank;
	private final String name;
	private final String department;
	
	public ImmutableClass(int rank, String name, String department) {
		this.rank = rank;
		this.name = name;
		this.department = department;
	}
	public int getRank() {
		return rank;
	}
	public String getName() {
		return name;
	}
	public String getDepartment() {
		return department;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new ImmutableClass(getRank(), getDepartment(), getName());
	}
}
